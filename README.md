# OpenAPI.tanna.dev

A hosted OpenAPI viewer, at [openapi.tanna.dev](https://openapi.tanna.dev).

## Usage

As well as the hosted version, linked above, you can run this completely locally:

```sh
npm i
npm run build
```

Then open `dist/index.html`.
